package de.mensaapp.htw.mensaapp;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import de.mensaapp.htw.mensaapp.db.FoodTableDbHelper;
import de.mensaapp.htw.mensaapp.db.RatingsTableDbHelper;
import de.mensaapp.htw.mensaapp.model.RatingModel;

/**
 * Created by Jakub on 17/06/16.
 */

@SuppressWarnings("ALL")
class DishesArrayAdapter extends ArrayAdapter<RatingModel> {


    private final Fragment dishesFragment;
    public static FrameLayout layout_MainMenu;

    FoodTableDbHelper fdb = new FoodTableDbHelper(getContext());
    RatingsTableDbHelper rdb = new RatingsTableDbHelper(getContext());


    DishesArrayAdapter(Context context, ArrayList<RatingModel> dishes, Fragment fragment) {
        super(context, R.layout.dishes_list_item, R.id.dishText, dishes);
        dishesFragment = fragment;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        final RatingModel ratingModel = getItem(position);
        final String dishString = ratingModel.getName();

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dishes_list_item, parent, false);
        }

        // Lookup view for data population
        TextView dishText = (TextView) convertView.findViewById(R.id.dishText);


        // Populate the data into the template view using the data object
        dishText.setText(ratingModel.getName());
        // Return the completed view to render on screen

        RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.RatingBar_mainScreen);
        ratingBar.setRating(ratingModel.getRating());


        // Set up onclicklisteners for list items
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View row = convertView;
        Button rateButton = (Button) row.findViewById(R.id.rateButton);
        rateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Cursor c = fdb.getFoodId(dishString);
                c.moveToFirst();
                int foodId = c.getInt(0);

                SharedPreferences sharedPref = getContext().getSharedPreferences("last_update", Context.MODE_PRIVATE);
                int last_update = sharedPref.getInt(String.valueOf(foodId), 0);

                if (last_update != 1) {

                    Intent intent = new Intent(getContext(), RatingPopup.class);
                    intent.putExtra("foodId", foodId);
                    getContext().startActivity(intent);

                    layout_MainMenu = (FrameLayout) dishesFragment.getActivity().findViewById(R.id.mainActivityLayout);
                    layout_MainMenu.getForeground().setAlpha(150);
                }
                else {
                    Toast.makeText(getContext(), "Du hast dieses Gericht schon bewertet", Toast.LENGTH_SHORT).show();

                }
            }
        });


        LinearLayout dishContent = (LinearLayout) row.findViewById(R.id.dishContent);
        dishContent.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                FoodTableDbHelper fdb = new FoodTableDbHelper(getContext());
                Cursor c = fdb.getFoodId(dishString);
                c.moveToFirst();
                int foodId = c.getInt(0);

                Intent intent = new Intent(getContext(), DetailScreenActivity.class);
                intent.putExtra("name", dishString);
                intent.putExtra("foodId", foodId);
                getContext().startActivity(intent);
            }
        });


        return convertView;
    }

}
