package de.mensaapp.htw.mensaapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.StringTokenizer;

import de.mensaapp.htw.mensaapp.model.RatingModel;

/**
 * Created by Jakub on 01/07/16.
 */

@SuppressWarnings("ALL")
class DishDetailsArrayAdapter extends ArrayAdapter<RatingModel> {


    //private final DetailScreenFragment dishDetailsFragment;


    DishDetailsArrayAdapter(Context context, ArrayList<RatingModel> ratings) {
        super(context, 0, ratings);
        //dishDetailsFragment = fragment;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        RatingModel ratingModel = getItem(position);


        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dish_rating_list_item, parent, false);
        }

        // Lookup view for data population
        TextView dishText = (TextView) convertView.findViewById(R.id.ratingText);
        RatingBar starRatingBar = (RatingBar) convertView.findViewById(R.id.MyRating);


        // Populate the data into the template view using the data object
        dishText.setText(ratingModel.comment);
        starRatingBar.setRating(ratingModel.rating);
        // Return the completed view to render on screen

        return convertView;
    }
}