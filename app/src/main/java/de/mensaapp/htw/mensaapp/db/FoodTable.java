package de.mensaapp.htw.mensaapp.db;

import android.provider.BaseColumns;

/**
 * Created by saubaer on 29.06.16.
 */

@SuppressWarnings({"ALL", "DefaultFileTemplate"})
final class FoodTable {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    FoodTable() { }

    /* Inner class that defines the table contents */
    public abstract static class Food_Entry implements BaseColumns {
        public static final String TABLE_FOOD = "food";
        public static final String COLUMN_FOOD_ID = "id";
        public static final String COLUMN_FOOD_NAME = "name";
        public static final String COLUMN_FOOD_PRICE = "price";
        public static final String COLUMN_FOOD_RATING = "rating";
        public static final String COLUMN_FOOD_OFFER_DATE = "offer_date";
        public static final String COLUMN_FOOD_INSERT_TIME = "insert_time";
    }
}



