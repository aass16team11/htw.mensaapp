package de.mensaapp.htw.mensaapp;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

public class DetailScreenActivity extends Activity implements DetailScreenFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_screen);


    }

    public void onFragmentInteraction(Uri uri) {
        //you can leave it empty
    }
}

