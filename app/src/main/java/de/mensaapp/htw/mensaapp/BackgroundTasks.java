package de.mensaapp.htw.mensaapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;

import de.mensaapp.htw.mensaapp.db.FoodTableDbHelper;
import de.mensaapp.htw.mensaapp.db.RatingsTableDbHelper;

/**
 * Created by saubaer on 01.07.16.
 */
@SuppressWarnings({"ALL", "DefaultFileTemplate"})
class BackgroundTasks extends AsyncTask<String, Integer, String> {

    private final Context ctx;
    private String foodId;


    BackgroundTasks(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        /*Authenticator.setDefault(new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("Android1", "test1".toCharArray());
            }
        });*/

    }

    protected String doInBackground(String... params) {


        String updateDatabase = ctx.getResources().getString(R.string.server_url) + "updateDatabase.php";

        String sendRating = ctx.getResources().getString(R.string.server_url) + "insertRating.php";

        String updateComments = ctx.getResources().getString(R.string.server_url) + "updateComments.php";


        String method = params[0];

        switch (method) {

            case "updateDatabase":

                String last_update = params[1];

                String JSON_STRING = null;

                String json_string = null;

                try {
                    URL url = new URL(updateDatabase);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    /*OutputStream OS = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS, "Utf-8"));
                    String data = URLEncoder.encode("last_update", "UTF-8") + "=" + URLEncoder.encode(last_update, "UTF-8");
                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    OS.close();*/

                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                    StringBuilder stringBuilder = new StringBuilder();

                    while ((JSON_STRING = bufferedReader.readLine()) != null) {
                        stringBuilder.append(JSON_STRING + "\n");
                    }

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    json_string = stringBuilder.toString().trim();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                    return "no connection";
                }

                JSONObject jsonObject;
                JSONArray jsonArray;


                try {
                    jsonObject = new JSONObject(json_string);
                    jsonArray = jsonObject.getJSONArray("server_response");

                    if (jsonArray.length() == 0) {
                        return "Database already up to date";
                    } else {
                        FoodTableDbHelper db = new FoodTableDbHelper(ctx);

                        db.updateDatabase(jsonArray);

                        return "Database updated";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    return "Database update failed";
                }


            case "updateComments":

                //String last_update_comments = params[1];

                String JSON_STRING_COMMENTS = null;

                String json_string_comments = null;

                try {
                    URL url = new URL(updateComments);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    /*OutputStream OS = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS, "Utf-8"));
                    String data = URLEncoder.encode("last_update", "UTF-8") + "=" + URLEncoder.encode(last_update, "UTF-8");
                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    OS.close();*/

                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                    StringBuilder stringBuilder = new StringBuilder();

                    while ((JSON_STRING_COMMENTS = bufferedReader.readLine()) != null) {
                        stringBuilder.append(JSON_STRING_COMMENTS + "\n");
                    }

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    json_string_comments = stringBuilder.toString().trim();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                    return "no connection";
                }

                JSONObject jsonObjectComments;
                JSONArray jsonArrayComments;


                try {
                    jsonObjectComments = new JSONObject(json_string_comments);
                    jsonArrayComments = jsonObjectComments.getJSONArray("server_response");

                    if (jsonArrayComments.length() == 0) {
                        return "Comments Database already up to date";
                    } else {
                        RatingsTableDbHelper db = new RatingsTableDbHelper(ctx);

                        db.updateComments(jsonArrayComments);

                        return "Comments Database updated";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    return "Comments Database update failed";
                }



            case "sendRating": {

                foodId = params[1];
                String rating = params[2];
                String comment = params[3];

                try {
                    URL url = new URL(sendRating);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    OutputStream OS = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS, "Utf-8"));
                    String data =
                                    URLEncoder.encode("food_id", "UTF-8") + "=" + URLEncoder.encode(foodId, "UTF-8") + "&" +
                                    URLEncoder.encode("rating", "UTF-8") + "=" + URLEncoder.encode(rating, "UTF-8") + "&" +
                                    URLEncoder.encode("comment", "UTF-8") + "=" + URLEncoder.encode(comment, "UTF-8");

                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    OS.close();

                    InputStream IS = httpURLConnection.getInputStream();
                    IS.close();

                    return "Rating sent";

                } catch (IOException e) {
                    e.printStackTrace();
                    return "no connection";
                }

            }

        }

        return "Success";
    }


    protected void onProgressUpdate(Integer... progress) {

    }


    protected void onPostExecute(String result) {
        Toast.makeText(ctx, result, Toast.LENGTH_SHORT).show();

        if (result.equals("Database updated")) {
            //sets the value of the sharedPreference lastupdate to the current timestamp
            SharedPreferences lastUpdate =
                    ctx.getSharedPreferences("last_update",
                            Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = lastUpdate.edit();
            editor.putString("last_update", String.valueOf(new Timestamp(System.currentTimeMillis())));
            editor.apply();
        }
        else if (result.equals("Rating sent")) {
            //sets the value of the sharedPreference lastupdate to the current timestamp
            SharedPreferences lastUpdate =
                    ctx.getSharedPreferences("last_update",
                            Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = lastUpdate.edit();
            editor.putInt(foodId, 1);
            editor.apply();
        }

    }
}
