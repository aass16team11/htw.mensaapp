package de.mensaapp.htw.mensaapp;

import android.app.DatePickerDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import de.mensaapp.htw.mensaapp.db.FoodTableDbHelper;
import de.mensaapp.htw.mensaapp.db.RatingsTableDbHelper;
import de.mensaapp.htw.mensaapp.model.RatingModel;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainScreenFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainScreenFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainScreenFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private OnFragmentInteractionListener mListener;


    private final ArrayList<String> contentList = new ArrayList<>();

    public final Calendar calendar = Calendar.getInstance();

    public MainScreenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainScreenFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainScreenFragment newInstance(String param1, String param2) {
        MainScreenFragment fragment = new MainScreenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            //String mParam1 = getArguments().getString(ARG_PARAM1);
            //String mParam2 = getArguments().getString(ARG_PARAM2);

        }*/





    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        View v  = inflater.inflate(R.layout.fragment_main_screen, container, false);

        setupDatePicker(v);

        Collections.sort(contentList);

        displayTodaysDishes(v, calendar.getTime());



        //Inflate the layout for this fragment
        return v;

    }


    private void displayTodaysDishes(View v, Date date) {
        FoodTableDbHelper dbHelper = new FoodTableDbHelper(getActivity());
        int mensa = getActivity().getIntent().getIntExtra("mensa", -1);
        Cursor allRatings;
        if (mensa == 0) {
            allRatings = dbHelper.getTodaysDishesTresko(date);
        }
        else {
            allRatings = dbHelper.getTodaysDishes(date);
        }

        ArrayList<RatingModel> arrayOfRatings = new ArrayList<>();
        // Create the adapter to convert the array to views
        DishesArrayAdapter adapter = new DishesArrayAdapter(getActivity(), arrayOfRatings, this);
        // Attach the adapter to a ListView

        while (allRatings.moveToNext()) {
            // Add item to adapter
            int foodId = allRatings.getInt(allRatings.getColumnIndex("id"));

            RatingsTableDbHelper rdb = new RatingsTableDbHelper(getActivity());
            rdb.getAverageRatingById(foodId);

            RatingModel ratingModel = new RatingModel(rdb.getAverageRatingById(foodId), null, allRatings.getString(allRatings.getColumnIndex("name")), 0);
            adapter.add(ratingModel);
        }


        ListView dishesList = (ListView) v.findViewById(R.id.dishesListView);

        dishesList.setAdapter(adapter);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void setupDatePicker(View v){

        final TextView chooseDateText = (TextView) v.findViewById(R.id.changeDateText);
        Calendar c = Calendar.getInstance();
        final SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault());
        String formattedDate = df.format(c.getTime());
        chooseDateText.setText(formattedDate);

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //updateDate();

                chooseDateText.setText(df.format(myCalendar.getTime()));
                displayTodaysDishes(getView(), myCalendar.getTime());
            }

        };


        chooseDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                        displayTodaysDishes(getView(), myCalendar.getTime());


            }
        });


        ImageButton previous = (ImageButton) v.findViewById(R.id.previousDayButton);
        ImageButton next = (ImageButton) v.findViewById(R.id.nextDayButton);

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    String dateString = (String) chooseDateText.getText();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault());
                    Calendar c = Calendar.getInstance();
                    c.setTime(sdf.parse(dateString));
                    c.add(Calendar.DATE, -1);


                    myCalendar.setTime(c.getTime());
                    chooseDateText.setText(sdf.format(myCalendar.getTime()));
                    displayTodaysDishes(getView(), myCalendar.getTime());



                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String dateString = (String) chooseDateText.getText();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault());
                    Calendar c = Calendar.getInstance();
                    c.setTime(sdf.parse(dateString));
                    c.add(Calendar.DATE, +1);


                    myCalendar.setTime(c.getTime());
                    chooseDateText.setText(sdf.format(myCalendar.getTime()));
                    displayTodaysDishes(getView(), myCalendar.getTime());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }





}
