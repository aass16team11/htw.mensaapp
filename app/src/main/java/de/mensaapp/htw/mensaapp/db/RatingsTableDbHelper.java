package de.mensaapp.htw.mensaapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by saubaer on 29.06.16.
 */
@SuppressWarnings("DefaultFileTemplate")
public class RatingsTableDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "RatingsTable.db";

    // Database creation SQL statement
    private static final String SQL_CREATE_ENTRIES = "create table "
            + RatingsTable.Ratings_Entry.TABLE_RATINGS
            + "("
            + RatingsTable.Ratings_Entry.COLUMN_RATINGS_ID + " integer primary key, "
            + RatingsTable.Ratings_Entry.COLUMN_RATINGS_FOOD_ID + " integer not null, "
            + RatingsTable.Ratings_Entry.COLUMN_RATINGS_RATING + " float not null, "
            + RatingsTable.Ratings_Entry.COLUMN_RATINGS_COMMENT + " text not null, "
            + FoodTable.Food_Entry.COLUMN_FOOD_INSERT_TIME + " timestamp not null "
            + ");";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + RatingsTable.Ratings_Entry.TABLE_RATINGS;


    public RatingsTableDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void clearDatabase(SQLiteDatabase db, String TABLE_NAME) {
        String clearDBQuery = "DELETE FROM "+TABLE_NAME;
        db.execSQL(clearDBQuery);
    }



    public void insertComment(int foodId, float rating, String comment) {

        Long tsLong = System.currentTimeMillis()/1000;
        String timestamp = tsLong.toString();

        ContentValues values = new ContentValues();

        values.describeContents();

        //values.put(FoodTable.Food_Entry.COLUMN_FOOD_ID, id);
        values.put(RatingsTable.Ratings_Entry.COLUMN_RATINGS_FOOD_ID, foodId);
        values.put(RatingsTable.Ratings_Entry.COLUMN_RATINGS_RATING, rating);
        values.put(RatingsTable.Ratings_Entry.COLUMN_RATINGS_COMMENT, comment);
        values.put(FoodTable.Food_Entry.COLUMN_FOOD_INSERT_TIME, timestamp);

        this.getWritableDatabase().insertOrThrow(RatingsTable.Ratings_Entry.TABLE_RATINGS, null, values);

        this.close();

    }


    public void updateComments(JSONArray jsonArray) {

        Double rating;
        String comment;
        int food_id;
        String insert_time;

        clearDatabase(this.getWritableDatabase(), "ratings");

        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject JO = jsonArray.getJSONObject(i);
                food_id = JO.getInt("food_id");
                comment = JO.getString("comment");
                rating = JO.getDouble("rating");
                insert_time = JO.getString("insert_time");


                ContentValues values = new ContentValues();

                values.describeContents();

                //values.put(FoodTable.Food_Entry.COLUMN_FOOD_ID, id);
                values.put(RatingsTable.Ratings_Entry.COLUMN_RATINGS_FOOD_ID, food_id);
                values.put(RatingsTable.Ratings_Entry.COLUMN_RATINGS_RATING, rating);
                values.put(RatingsTable.Ratings_Entry.COLUMN_RATINGS_COMMENT, comment);
                values.put(FoodTable.Food_Entry.COLUMN_FOOD_INSERT_TIME, insert_time);

                this.getWritableDatabase().insert(RatingsTable.Ratings_Entry.TABLE_RATINGS, null, values);

                this.close();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }



    public Cursor getAllRatings() {

        String query = "SELECT * FROM ratings";
        return this.getReadableDatabase().rawQuery(query, null);
    }


    public Cursor getRatingById (int id) {
        String query = "SELECT * FROM ratings WHERE food_id = \"" + id + "\"";
        return this.getReadableDatabase().rawQuery(query, null);
    }


    public Float getAverageRatingById (int id) {

        String query = "SELECT * FROM ratings WHERE food_id = " + id;

        Float average = 0.0f;

        Cursor averageRating = this.getReadableDatabase().rawQuery(query, null);

        while (averageRating.moveToNext()) {
            average += averageRating.getFloat(averageRating.getColumnIndex("rating"));

        }
        if (average != 0.0f) {
            average = average/averageRating.getCount();
        }

        return average;
    }
}
