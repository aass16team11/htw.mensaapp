package de.mensaapp.htw.mensaapp;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.mensaapp.htw.mensaapp.db.RatingsTableDbHelper;
import de.mensaapp.htw.mensaapp.model.RatingModel;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailScreenFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetailScreenFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailScreenFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private OnFragmentInteractionListener mListener;

    private String[] ratings = {
            "Mensa HTW Wilhelminenhof",
            "Mensa HTW Treskowallee"
    };
    private ArrayList<String> contentList = new ArrayList<>();


    public DetailScreenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailScreenFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailScreenFragment newInstance(final String param1, final String param2) {
        DetailScreenFragment fragment = new DetailScreenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_detail_screen, container, false);


        CharSequence name = getActivity().getIntent().getStringExtra("name");
        int foodId = getActivity().getIntent().getIntExtra("foodId", 0);


        TextView t = (TextView) v.findViewById(R.id.dishTitle);
        t.setText(name);

        displayAverageRating(v, foodId);
        displayRatings(v, foodId);

        setUpClickListeners(v);

        return v;
    }

    private void displayAverageRating(View v, int foodId) {

        RatingsTableDbHelper rdb = new RatingsTableDbHelper(getActivity());
        Float average = rdb.getAverageRatingById(foodId);


        TextView averageTextView = (TextView) v.findViewById(R.id.averageRating);
        averageTextView.setText(String.valueOf(Math.round(average * 100.0) / 100.0));

        RatingBar ratingBar = (RatingBar) v.findViewById(R.id.MyRating);
        ratingBar.setRating(average);
    }

    private void displayRatings(View v, int foodId) {

        RatingsTableDbHelper rdb = new RatingsTableDbHelper(getActivity());
        Cursor allRatings = rdb.getRatingById(foodId);

        ArrayList<RatingModel> arrayOfUsers = new ArrayList<>();
        // Create the adapter to convert the array to views
        DishDetailsArrayAdapter adapter = new DishDetailsArrayAdapter(getActivity(), arrayOfUsers);
        // Attach the adapter to a ListView

        while (allRatings.moveToNext()) {
            // Add item to adapter
            RatingModel ratingModel = new RatingModel(allRatings.getFloat(allRatings.getColumnIndex("rating")), allRatings.getString(allRatings.getColumnIndex("comment")), null, foodId);
            adapter.add(ratingModel);
        }


            ListView chooseMensaList = (ListView) v.findViewById(R.id.dishesListView);

            chooseMensaList.setAdapter(adapter);
    }




   private void setUpClickListeners(View v) {
       ImageButton backToMainScreenButton = (ImageButton) v.findViewById(R.id.backToMainScreenButton);
       backToMainScreenButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               getActivity().onBackPressed();

           }
       });
   }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
