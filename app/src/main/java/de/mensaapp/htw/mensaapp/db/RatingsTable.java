package de.mensaapp.htw.mensaapp.db;

import android.provider.BaseColumns;

/**
 * Created by saubaer on 29.06.16.
 */

@SuppressWarnings({"ALL", "DefaultFileTemplate"})
final class RatingsTable {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    RatingsTable() { }

    /* Inner class that defines the table contents */
    public abstract static class Ratings_Entry implements BaseColumns {
        public static final String TABLE_RATINGS = "ratings";
        public static final String COLUMN_RATINGS_ID = "id";
        public static final String COLUMN_RATINGS_FOOD_ID = "food_id";
        public static final String COLUMN_RATINGS_RATING = "rating";
        public static final String COLUMN_RATINGS_COMMENT = "comment";
        public static final String COLUMN_FOOD_INSERT_TIME = "insert_time";
    }
}



