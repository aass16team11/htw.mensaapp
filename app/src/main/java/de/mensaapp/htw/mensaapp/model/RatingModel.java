package de.mensaapp.htw.mensaapp.model;

/**
 * Created by saubaer on 14.07.16.
 */
public class RatingModel {

        public int id;
        public float rating;
        public String comment;
        public String name;

        public RatingModel(float rating, String comment, String name, int id) {
            this.rating = rating;
            this.comment = comment;
            this.name = name;
            this.id = id;

        }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
