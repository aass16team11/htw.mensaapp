package de.mensaapp.htw.mensaapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import de.mensaapp.htw.mensaapp.db.RatingsTableDbHelper;

public class RatingPopup extends Activity{


    private EditText ratingEditText;
    private RatingBar starRatingBar;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_popup);


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        double width = dm.widthPixels * 0.8;
        double height = dm.heightPixels * 0.55;

        getWindow().setLayout((int)width, (int)height);


        setFinishOnTouchOutside(false); //prevents closing of popup when something outside of it is touched


        registerButtonClickListeners();

        ratingEditText = (EditText) findViewById(R.id.ratingTextInputField);
        starRatingBar = (RatingBar) findViewById(R.id.ratingBar);


    }

    private void registerButtonClickListeners(){
        Button submitRating = (Button) findViewById(R.id.submitRatingButton);
        Button cancelRating = (Button) findViewById(R.id.cancelRatingButton);

        submitRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    submit();



                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        cancelRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    cancel();



                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void submit(){


        String ratingText = ratingEditText.getText().toString();
        float ratingValue = starRatingBar.getRating();
        int foodId = getIntent().getIntExtra("foodId", 0);

        if(ratingValue == 0.0f){
            Toast.makeText(getApplicationContext(), R.string.rate_dish_submit_error_stars, Toast.LENGTH_SHORT).show();
            return;
        }

        //TODO send ratingText and ratingValue to the correct meal on the server
        RatingsTableDbHelper rdb = new RatingsTableDbHelper(getApplicationContext());

        rdb.insertComment(foodId, ratingValue, ratingText);

        Toast.makeText(getApplicationContext(), R.string.rate_dish_submitted_toast, Toast.LENGTH_SHORT).show();

        String method = "sendRating";
        BackgroundTasks backgroundTask = new BackgroundTasks(this);
        backgroundTask.execute(method, String.valueOf(foodId), String.valueOf(ratingValue), ratingText);

        Intent intent = new Intent(getApplicationContext(), MainScreenActivity.class);
        startActivity(intent);
    }

    private void cancel(){
        //Toast.makeText(getApplicationContext(), R.string.rate_dish_canceled_toast, Toast.LENGTH_SHORT).show();
        closePopup();

    }

    @Override
    public void onBackPressed() {
        closePopup();
    }

    private void closePopup(){
        FrameLayout layout_MainMenu = DishesArrayAdapter.layout_MainMenu;
        layout_MainMenu.getForeground().setAlpha(0);
        finish();
    }





}
