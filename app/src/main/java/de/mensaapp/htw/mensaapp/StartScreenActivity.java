package de.mensaapp.htw.mensaapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;



import java.util.ArrayList;

import de.mensaapp.htw.mensaapp.db.FoodTableDbHelper;

import static de.mensaapp.htw.mensaapp.MainScreenActivity.convertSeparators;

public class StartScreenActivity extends AppCompatActivity {

    private final static String TAG = StartScreenActivity.class.getSimpleName();
    private final String [] aktienlisteArray = {
            "Mensa HTW Wilhelminenhof",
            "Mensa HTW Treskowallee"
    };

    private final ArrayList<String> contentList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);

        Collections.addAll(contentList, aktienlisteArray);

        Collections.sort(contentList);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, contentList);

        ListView chooseMensaList = (ListView) findViewById(R.id.mensaListView);

        chooseMensaList.setAdapter(adapter);


        chooseMensaList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
            {

                Toast.makeText(getApplicationContext(), R.string.toast_mensa_chosen, Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getApplicationContext(), MainScreenActivity.class);
                intent.putExtra("mensa", position);
                startActivity(intent);

            }
        });

        if (isNetworkAvailable()) {
            //updates The database
            try {
                updateDatabase();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private void updateDatabase() throws IOException {

        BackgroundTasks commentBackgroundTask = new BackgroundTasks(this);
        commentBackgroundTask.execute("updateComments");

        String defaultValue = "0000-00-00 00:00:00";

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("last_update", Context.MODE_PRIVATE);
        String last_update = sharedPref.getString("last_update", defaultValue);

        String convertedLastUpdate = convertSeparators(last_update);

        Calendar c=Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
        c.set(Calendar.HOUR_OF_DAY,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.add(Calendar.DATE,-7);
        //performs the database update if the last update was before last sunday
        if (Timestamp.valueOf(convertedLastUpdate).before(c.getTime())) {

            BackgroundTasks backgroundTask = new BackgroundTasks(this);
            backgroundTask.execute("updateDatabase", String.valueOf(last_update));

        }
        else {
            Toast.makeText(getApplicationContext(), "Database already updated", Toast.LENGTH_SHORT).show();
        }
    }




}
