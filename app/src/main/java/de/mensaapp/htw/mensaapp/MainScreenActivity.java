package de.mensaapp.htw.mensaapp;

import android.app.Activity;

import android.content.Context;

import android.content.Intent;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;

import de.mensaapp.htw.mensaapp.Helpers.ShakeDetector;

public class MainScreenActivity extends Activity implements MainScreenFragment.OnFragmentInteractionListener{

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;

    //Converts the separators of the last_update String to perform the convert to timestamp
    static String convertSeparators(String input) {
        char[] chars = input.toCharArray();
        chars[10] = ' ';
        chars[13] = ':';
        chars[16] = ':';
        return new String(chars);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        FrameLayout layout_MainMenu = (FrameLayout) this.findViewById(R.id.mainActivityLayout);
        layout_MainMenu.getForeground().setAlpha(0);

        initShakeDetector();

    }


    private void initShakeDetector(){
        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
				/*
				 * The following method, "handleShakeEvent(count):" is a stub //
				 * method you would use to setup whatever you want done once the
				 * device has been shook.
				 */
                handleShakeEvent(count);
            }
        });
    }

    private  void handleShakeEvent(int count){

        //Toast.makeText(getApplicationContext(), "Shakes: " + count, Toast.LENGTH_SHORT).show();

        Toast.makeText(getApplicationContext(), R.string.toast_rechoose_mensa, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(getApplicationContext(), StartScreenActivity.class);
        //intent.putExtra
        startActivity(intent);

    }

    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }


    public void onFragmentInteraction(Uri uri){
        //you can leave it empty
    }






}
