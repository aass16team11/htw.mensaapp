package de.mensaapp.htw.mensaapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by saubaer on 29.06.16.
 */
@SuppressWarnings("DefaultFileTemplate")

public class FoodTableDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "FoodTable.db";

    private static String DB_NAME = "FoodTable.db";
    private final Context myContext;
    private boolean mInvalidDatabaseFile = false;



    // Database creation SQL statement
    private static final String SQL_CREATE_ENTRIES = "create table "
            + FoodTable.Food_Entry.TABLE_FOOD
            + "("
            + FoodTable.Food_Entry.COLUMN_FOOD_ID + " integer primary key, "
            + FoodTable.Food_Entry.COLUMN_FOOD_NAME + " text not null, "
            + FoodTable.Food_Entry.COLUMN_FOOD_PRICE + " text not null, "
            + FoodTable.Food_Entry.COLUMN_FOOD_RATING + " float not null, "
            + FoodTable.Food_Entry.COLUMN_FOOD_OFFER_DATE + " date not null, "
            + FoodTable.Food_Entry.COLUMN_FOOD_INSERT_TIME + " timestamp not null "
            + ");";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FoodTable.Food_Entry.TABLE_FOOD;


    public FoodTableDbHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        this.myContext = context;

        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();
            if (db != null) {
                db.close();
            }

            if (mInvalidDatabaseFile) {
                copyDatabase();
            }
        } catch (Exception e) {
            Log.d("DATABASE", e.getClass() + "//"+e.getMessage());
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
    }

    private void copyDatabase() throws IOException{
        //Open your local db as the input stream
        try {
            String ASSET_NAME = "FoodTable.db";
            InputStream myInput = myContext.getAssets().open(ASSET_NAME);

            //Open the empty db as the output stream
            OutputStream myOutput = new FileOutputStream(myContext.getDatabasePath(DB_NAME));

            //transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer))>0){
                myOutput.write(buffer, 0, length);
            }

            //Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();

            setDatabaseVersion();
            mInvalidDatabaseFile = false;
        } catch (IOException e) {
            throw new Error("Error creating database: "+ e.getClass()+" // "+e.getMessage());
        }
    }

    private void setDatabaseVersion() {
        SQLiteDatabase db = null;
        try {
            db = SQLiteDatabase.openDatabase(myContext.getDatabasePath(DB_NAME).getAbsolutePath(), null,
                    SQLiteDatabase.OPEN_READWRITE);
            db.execSQL("PRAGMA user_version = " + DATABASE_VERSION);
        } catch (SQLiteException ignored) {
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
        mInvalidDatabaseFile = true;
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        mInvalidDatabaseFile = true;
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void clearDatabase(SQLiteDatabase db, String TABLE_NAME) {
        String clearDBQuery = "DELETE FROM "+TABLE_NAME;
        db.execSQL(clearDBQuery);
    }

    public void updateDatabase(JSONArray jsonArray) {

        int id;
        String name;
        String price;
        double rating;
        String offer_date;
        String insert_time;

        clearDatabase(this.getWritableDatabase(), "food");

        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject JO = jsonArray.getJSONObject(i);
                id = JO.getInt("id");
                name = JO.getString("name");
                price = JO.getString("price");
                rating = JO.getDouble("rating");
                offer_date = JO.getString("offer_date");
                insert_time = JO.getString("insert_time");


                ContentValues values = new ContentValues();

                values.describeContents();

                values.put(FoodTable.Food_Entry.COLUMN_FOOD_ID, id);
                values.put(FoodTable.Food_Entry.COLUMN_FOOD_NAME, name);
                values.put(FoodTable.Food_Entry.COLUMN_FOOD_PRICE, price);
                values.put(FoodTable.Food_Entry.COLUMN_FOOD_RATING, rating);
                values.put(FoodTable.Food_Entry.COLUMN_FOOD_OFFER_DATE, offer_date);
                values.put(FoodTable.Food_Entry.COLUMN_FOOD_INSERT_TIME, insert_time);

                this.getWritableDatabase().insertOrThrow(FoodTable.Food_Entry.TABLE_FOOD, null, values);

                this.close();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public Cursor getFoodId(String name) {

        String query = "SELECT id FROM food WHERE name = \"" + name + "\"";
        return this.getReadableDatabase().rawQuery(query, null);

    }


    //Takes an id of an origin and returns all the related names
    public Cursor getAllDishes() {

        String query = "SELECT name, price, offer_date FROM food";
        return this.getReadableDatabase().rawQuery(query, null);
    }


    public Cursor getTodaysDishes(Date date) {

        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMAN);

        String day = sdf.format(date);

        String query = "SELECT id, name, price, offer_date FROM food WHERE offer_date = \"" + day + "\"";
        return this.getReadableDatabase().rawQuery(query, null);
    }

    public Cursor getTodaysDishesTresko(Date date) {

        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMAN);

        String day = sdf.format(date);

        String query = "SELECT id, name, price, offer_date FROM food_tresko WHERE offer_date = \"" + day + "\"";
        return this.getReadableDatabase().rawQuery(query, null);
    }


    public Cursor getDetailDish(CharSequence name) {

        String query = "SELECT id, name, price, rating FROM food WHERE name = \"" + name + "\"";
        return this.getReadableDatabase().rawQuery(query, null);
    }
}
